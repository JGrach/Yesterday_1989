const GAME_GRID = GRID_CREATION()
GAME_GRID.toInit(GAME_GRID_SIZE)

var SCORE = 0

function game() {
  if (!GAME_GRID.isContinuingGrid()) {
    VIEWER.toGameOver()
    return false
  }

  const form = FORM_CREATION()
  form.toInit()

  VIEWER.toResult()
  VIEWER.toDisplay()

  function onKeyPress(e) {
    switch (e.key) {
      case 'a':
        form.toDrawChanging(ANGLE_NEGATIVE)
        break
      case 'e':
        form.toDrawChanging(ANGLE_POSITIVE)
        break
      case 'd':
        form.toMove(RIGHT)
        break
      case 'q':
        form.toMove(LEFT)
        break
      case 's':
        form.toMove(DOWN)
        break
    }
    VIEWER.toDisplay()
  }

  function down() {
    if (!form.toMove(DOWN)) {
      return false
    }
    VIEWER.toDisplay()
    return true
  }

  window.addEventListener('keypress', onKeyPress)

  const downing = setInterval(function() {
    if (!down()) {
      clearInterval(downing)
      const lineDeletion = GAME_GRID.toDeleteFullLine()
      if (lineDeletion > 0) {
        SCORE += lineDeletion
        if (TIME_TO_DOWN > 150) {
          TIME_TO_DOWN = TIME_TO_DOWN - SCORE * 10
        }
      }
      window.removeEventListener('keypress', onKeyPress)
      game()
    }
  }, TIME_TO_DOWN)
}

VIEWER.toDisplay()
game()
