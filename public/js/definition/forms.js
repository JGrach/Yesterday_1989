var FORM_NUMBER = 0

function FORM_CREATION() {
  const square = {
    toInit: function(position, identity) {
      this.position = position
      this.formId = identity.formId
      this.selfId = identity.selfId
      this.context = GAME_GRID
      this.context.toPlainSquare(this)
    },
    isMoveAvailable: function(direction) {
      return this.context.isSquareEmpty(this, direction)
    },
    toDelete: function() {
      this.context.toEmptySquare(this.position.x, this.position.y)
    },
    toMove: function(direction) {
      this.position.x += direction.x
      this.position.y += direction.y
      if (!this.context.toPlainSquare(this)) {
        return new Error('Uncaught to plain square')
      }
      return true
    },
  }

  const form = {
    toInit: function() {
      this.formId = FORM_NUMBER
      FORM_NUMBER++
      const drawIndex = GET_RANDOM_INT(0, Object.keys(DRAWS).length)
      this.drawKey = Object.keys(DRAWS)[drawIndex]
      this.globalDraw = DRAWS[this.drawKey]
      this.angle = 0
      this.draw = this.globalDraw[this.angle].slice()
      this.position = START_POSITION
      this.squares = []

      this.squares = this.draw.map(
        function(squareInit, index) {
          const newSquare = Object.create(square)
          const position = {
            x: squareInit.x + this.position.x,
            y: squareInit.y + this.position.y,
          }
          newSquare.toInit(position, { formId: this.formId, selfId: index })
          return newSquare
        }.bind(this)
      )
    },
    toChangeAngle: function(variance) {
      var newAngle = this.angle + variance
      return newAngle >= this.globalDraw.length
        ? 0
        : newAngle < 0
          ? this.globalDraw.length - 1
          : newAngle
    },
    toMove: function(direction) {
      const isMoving = this.squares.reduce(function(isMoving, square) {
        return isMoving ? square.isMoveAvailable(direction) : false
      }, true)
      if (isMoving) {
        this.squares.map(function(square) {
          square.toDelete()
        })
        this.squares.map(function(square) {
          square.toMove(direction)
        })
        this.position = this.squares[0].position
        return true
      }
      return false
    },
    isDrawChanging: function(newDraw) {
      return this.squares.reduce(
        function(isMoving, square, i) {
          const firstStep = {
            x: -this.draw[i].x,
            y: -this.draw[i].y,
          }
          const secondStep = {
            x: firstStep.x + newDraw[i].x,
            y: firstStep.y + newDraw[i].y,
          }
          return isMoving
            ? square.isMoveAvailable(firstStep)
              ? square.isMoveAvailable(secondStep)
              : false
            : false
        }.bind(this),
        true
      )
    },
    toDrawChanging: function(angle) {
      const newAngle = this.toChangeAngle(angle)
      if (newAngle == this.angle) {
        return false
      }
      const newDraw = this.globalDraw[newAngle]
      if (!this.isDrawChanging(newDraw)) {
        return false
      }
      this.angle = newAngle
      this.squares.map(function(square) {
        square.toDelete()
      })
      this.squares.map(
        function(square, i) {
          const firstStep = {
            x: -this.draw[i].x,
            y: -this.draw[i].y,
          }
          const secondStep = {
            x: newDraw[i].x,
            y: newDraw[i].y,
          }
          square.toMove(firstStep)
          square.toMove(secondStep)
        }.bind(this)
      )
      this.draw = this.globalDraw[this.angle].slice()
      return true
    },
  }

  return Object.create(form)
}
