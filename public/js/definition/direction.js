const DOWN = {
  x: 0,
  y: 1,
}

const RIGHT = {
  x: 1,
  y: 0,
}

const LEFT = {
  x: -1,
  y: 0,
}

const ANGLE_POSITIVE = 1

const ANGLE_NEGATIVE = -1
