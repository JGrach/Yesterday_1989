function GRID_CREATION() {
  const grid = {
    toInit: function(length) {
      this.length = length
      this.empty = false
      this.lines = []
      for (var i = 0; i < this.length.y; i++) {
        this.lines.push(this.createLine())
      }
    },
    createLine: function() {
      var line = []
      for (var i = 0; i < this.length.x; i++) {
        line.push(this.empty)
      }
      return line
    },
    toPlainSquare: function(square) {
      if (!this.lines[square.position.y][square.position.x]) {
        this.lines[square.position.y][square.position.x] = square
        return true
      }
      return false
    },
    toEmptySquare: function(x, y) {
      this.lines[y][x] = this.empty
      return true
    },
    isSquareEmpty: function(square, direction) {
      const newPosition = {
        y: square.position.y + direction.y,
        x: square.position.x + direction.x,
      }
      if (
        newPosition.y < 0 ||
        newPosition.x < 0 ||
        newPosition.y >= GAME_GRID_SIZE.y ||
        newPosition.x >= GAME_GRID_SIZE.x
      ) {
        return false
      }
      return (
        this.lines[newPosition.y][newPosition.x] === this.empty ||
        this.lines[newPosition.y][newPosition.x].formId === square.formId
      )
    },
    toDeleteFullLine: function() {
      return this.lines.reduce(
        function(numberDeleted, line, y) {
          const isFullLine = line.reduce(function(isFullLine, x) {
            return isFullLine ? !!x : false
          }, true)
          if (isFullLine) {
            this.lines.splice(y, 1)
            this.lines.unshift(this.createLine())
            return numberDeleted +1
          }
          return numberDeleted
        }.bind(this),
        0
      )
    },
    isContinuingGrid: function() {
      return this.lines[0].reduce(function(isContinuingGrid, x) {
        return isContinuingGrid ? x === false : false
      }, true)
    },
  }

  return Object.create(grid)
}
